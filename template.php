<?php
/**
* Any of the functions below can be overriden by your theme!
* Simply copy and page the whole function into your subtheme's template.php and change the "runvsc" to the name of your subtheme.
* How do you create a subtheme? Simple copy the "startvsc" folder, give your theme a name, and  * replace any text that is "startvsc" with the name of your theme!
*/
/**
* Adds a default set of helper variables for preprocess functions and
* templates. This comes in before any other preprocess function which makes
* it possible to be used in default theme implementations (non-overriden
* theme functions).
* you might be wondering where the actual "preprocess" drupal functions are...
* they are declared in your sub theme -> look in startvsc!
*/
function runvsc_preprocess(& $variables, $hook) {
  global $user ;
  static $count = array() ;
// See "runvsc_preprocess_block()" which provides the same feature specific to blocks.
  $count[$hook] = isset ($count[$hook]) && is_int($count[$hook]) ? $count[$hook] : 1 ;
  $variables['zebra'] = ($count[$hook] % 2) ? 'odd' : 'even' ;
  $variables['id'] = $count[$hook]++;
// Tell all templates where they are located.
  $variables['directory'] = path_to_theme() ;
// Set default variables that depend on the database.
  $variables['is_admin'] = false ;
  $variables['is_front'] = false ;
  $variables['logged_in'] = false ;
  if ($variables['db_is_active'] = db_is_active() && !defined('MAINTENANCE_MODE')) {
// Check for administrators.
    if (user_access('access administration pages')) {
      $variables['is_admin'] = true ;
    }
// Flag front page status.
    $variables['is_front'] = drupal_is_front_page() ;
// Tell all templates by which kind of user they're viewed.
    $variables['logged_in'] = ($user->uid > 0) ;
// Provide user object to all templates
    $variables['user'] = $user ;
  }
}

/**
* Create functions that we will find useful throughout
*/
/**
* runvsc_id_safe makes sure that any name / title / string entered into the system won't blow it up
*/
function runvsc_id_safe($string) {
  if (is_numeric($string { 0 })) {
    $string = 'n' . $string ;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string)) ;
}

/**
* Need to find a value in an array? this little ditty will tell you if a value is in there.
*/
function runvsc_is_in_array($needle, $haystack) {
  foreach ($haystack as $item) {
    if (!is_array($item)) {
      if ($item == $needle)
        return true ;
      else
        continue ;
    }
    if (in_array($needle, $item))
      return true ;
    else
      if (runvsc_is_in_array($needle, $item))
        return true ;
  }
  return false ;
}

/**
* Takes an array and implodes it into a string with spaces in it.
*/
function runvsc_implode_array($array, $space = " ") {
  $output = implode($space, $array) ;
  return $output ;
}

/**
* This is how Drupal builds the favicon
*/
function runvsc_build_favicon() {
// Add favicon
  if (theme_get_setting('toggle_favicon')) {
    drupal_set_html_head('<link rel="shortcut icon" href="' . check_url(theme_get_setting('favicon')) . '" type="image/x-icon" />') ;
  }
}

function runvsc_feed_icon($url, $title) {
  return '<a href="' . check_url($url) . '" class="feed-icon"></a>' ;
}

/**
* The function below is not used. but I put it here for reference
*/
function runvsc_build_regions(& $variables) {
  global $theme ;
// Populate all block regions.
  $regions = system_region_list($theme) ;
// Load all region content assigned via blocks.
  foreach (array_keys($regions) as $region) {
// Prevent left and right regions from rendering blocks when 'show_blocks' == FALSE.
    if (!(!$variables['show_blocks'] && ($region == 'left' || $region == 'right'))) {
      $blocks = theme('blocks', $region) ;
    }
    else {
      $blocks = '' ;
    }
// Assign region to a region variable.
    isset ($variables[$region]) ? $variables[$region] .= $blocks : $variables[$region] = $blocks ;
  }
}

/**
* Sets up the layout variables - note that drupal defines the right and left regions as "right" and "left" by default
*/
function runvsc_build_layout_variables(& $variables) {
// Set up layout variable.
  $variables['layout'] = 'none' ;
  if (!empty ($variables['left'])) {
    $variables['layout'] = 'left' ;
  }
  if (!empty ($variables['right'])) {
    $variables['layout'] = ($variables['layout'] == 'left') ? 'both' : 'right' ;
  }
}

/**
* drupal doesn't have a function to generate default div tags - 
* you would thinks that theme() would have something like that ... right? - johnvsc
* 
* $id      = pass a unique id
* $classes = an array of class names -> build by assignment = $classes[] = "new-class"
* $output  = the content that you are passing
* $inner   = pass "true" if you want to create an inner div -> this is used for padding esp within a 960.gs!
*/
function runvsc_build_div_markup($id = "region", $classes, $output, $inner = null) {
  $classesPrint = ($classes[0]) ? 'class="' . runvsc_implode_array($classes, " ") . '"' : "" ;
  if ($inner) {
    $output = runvsc_build_div_markup($id, $classes, '<div id="' . $id . '-inner">' . $output . '</div>') ;
  }
  else {
    $output = '<div id="' . $id . '" ' . $classesPrint . '>' . $output . '</div>' ;
  }
  return $output ;
}

/**
* a more generic version that above - you don't have to pass a class as an array - just a string
*
* $tag     = span, h1-h6, p : you get the idea
* $id      = pass a unique id
* $classes = this could be something like this "clear-all float-left new-byline"
* $output  = the content that you are passing
*/
function runvsc_build_tag_markup($tag, $id, $classes, $output) {
  $id = ($id) ? 'id="'.$id.'" ': '' ;
  $classes = ($classes) ? 'class="'.$classes.'" ' : '' ;
  $output = '<' . $tag . ' ' . $id.$classes . '>' . $output . '</' . $tag . '>' ;
  return $output ;
}


/* sample page */
/**
* functions that build all page.tpl.php container variables
*/
function runvsc_complie_body_classes(& $variables, $addclass = '') {
// Compile a list of classes that are going to be applied to the body element.
// This allows advanced theming based on context (home page, node of certain type, etc.).
  $body_classes = array() ;
// Add a class that tells us whether we're on the front page or not.
  $body_classes[] = $variables['is_front'] ? 'front' : 'not-front' ;
// Add a class that tells us whether the page is viewed by an authenticated user or not.
  $body_classes[] = $variables['logged_in'] ? 'logged-in' : 'not-logged-in' ;
  $body_classes[] = $variables['admin'] ? 'admin-toolbar' : '' ;
  $body_classes[] = $variables['title'] ? 'title-' . form_clean_id(drupal_strtolower($variables['title'])) : 'no-title' ;
  $body_classes[] = (arg(1)) ? 'path-' . form_clean_id(runvsc_implode_array(arg(), "-")) : 'no-nid' ;
// Add arg(0) to make it possible to theme the page depending on the current page
// type (e.g. node, admin, user, etc.). To avoid illegal characters in the class,
// we're removing everything disallowed. We are not using 'a-z' as that might leave
// in certain international characters (e.g. German umlauts).
//$body_classes[] = preg_replace('![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s', '', 'page-'. form_clean_id(drupal_strtolower(arg(0))));
// If on an individual node page, add the node type.
  if (isset ($variables['node']) && $variables['node']->type) {
    $body_classes[] = 'node-type-' . $variables['node']->type ;
  }
// Add information about the number of sidebars.
  if ($variables['layout'] == 'both') {
    $body_classes[] = 'two-sidebars' ;
  }
  elseif ($variables['layout'] == 'none') {
    $body_classes[] = 'no-sidebars' ;
  }
  else {
    $body_classes[] = 'one-sidebar sidebar-' . $variables['layout'];
  }
  $body_classes[] = $addclass ;
// Implode with spaces.
  $variables['body_classes'] = 'class="' . runvsc_implode_array($body_classes) . '"' ;
}

/**
* In the page.tpl.php, you will see that I didn't send the major regions through a function. I did that because for those regions, class would be dynamic ... and creating the function below to handle the class assignments worked better.
*/
function runvsc_compile_region_classes(& $variables) {
  $variables['page_classes'] = runvsc_build_page_classes($variables['layouts']) ;
  $variables['header_classes'] = runvsc_build_header_classes($variables['layouts']) ;
  $variables['container_classes'] = runvsc_build_container_classes($variables['layouts']) ;
  $variables['main_classes'] = runvsc_build_main_classes($variables) ;
  $variables['footer_classes'] = runvsc_build_footer_classes($variables['layouts']) ;
}

/**
* Duplicated in the starter theme; I have this here for reference.
* This allows you to set up the 960 classes for the default regions very quickley.
* "other" classes that you can add are alpha, omega, push and pulls.
*  This is documented way better in the starter theme
*/
function runvsc_define_layouts() {
  $container = 12 ;
/* $container = "16" */
  $layout_array = array() ;
  $layout_array["page"] = array('container' => $container,) ;
  $layout_array["right"] = array('container' => $container, 'grid' => 2, 'other' => 'omega',) ;
  $layout_array["left"] = array('container' => $container, 'grid' => 3, 'other' => 'alpha',) ;
  $layout_array["primary"] = array('container' => $container, 'grid' => 2, 'other' => '',) ;
  $layout_array["secondary"] = array('container' => $container, 'grid' => 2, 'other' => '',) ;
  $layout_array["site_name"] = array('container' => $container, 'grid' => 4, 'other' => '',) ;
  $layout_array["logo"] = array('container' => $container, 'grid' => 2, 'other' => 'alpha',) ;
  $layout_array["default"] = array('container' => $container, 'grid' => $container, 'other' => '',) ;
  return $layout_array ;
}

/**
* The layout classes function below give you the opportunity to add, adjust class that will be in the respective region.
* You probably won't ever have to touch "runvsc_build_layout_classes".
*/
function runvsc_build_layout_classes($layout_array, $region = 'default') {
  $region_name = $region ;
// drupal_set_message("region name is ".$region_name);
  $layout[] = (isset ($layout_array[$region_name]['container'])) ? "container-" . $layout_array[$region_name]['container'] : '' ;
  $layout[] = (isset ($layout_array[$region_name]['grid'])) ? "grid-" . $layout_array[$region_name]['grid'] : '' ;
  $layout[] = (isset ($layout_array[$region_name]['other'])) ? $layout_array[$region_name]['other'] : '' ;
  $output = runvsc_implode_array($layout) ;
  return $output ;
}

/**
* To add classes to anyone of the regions below, pass a string of classes you want to add
* simply seperated by spaces!
*/
function runvsc_build_page_classes($layouts, $addclass = '') {
  $classes[] = runvsc_build_layout_classes($layouts, 'page') ;
  $classes[] = $addclass ;
  $output = 'class="' . runvsc_implode_array($classes) . '"' ;
  return $output ;
}

function runvsc_build_header_classes($layouts, $addclass = '') {
  $classes[] = runvsc_build_layout_classes($layouts) ;
  $classes[] = $addclass ;
  $output = 'class="' . runvsc_implode_array($classes) . '"' ;
  return $output ;
}

function runvsc_build_container_classes($layouts, $addclass = '') {
  $classes[] = runvsc_build_layout_classes($layouts) ;
  $classes[] = $addclass ;
  $output = 'class="' . runvsc_implode_array($classes) . '"' ;
  return $output ;
}

function runvsc_build_main_classes(& $variables, $addclass = '') {
  $left = $variables['left'];
  $left_layout = $variables['layouts']['left']['grid'];
  $right = $variables['right'];
  $right_layout = $variables['layouts']['right']['grid'];
  $main_layout = $variables['layouts']['default']['container'];
  $container = "container-" . $main_layout ;
  if ($left && $right) {
    $value = $main_layout - $right_layout - $left_layout ;
    $grid = "grid-" . $value ;
    $classes = array($container, $grid) ;
  }
  else
    if ($left) {
      $value = $main_layout - $left_layout ;
      $grid = "grid-" . $value ;
      $classes = array($container, $grid) ;
    }
    else
      if ($right) {
        $value = $main_layout - $right_layout ;
        $grid = "grid-" . $value ;
        $classes = array($container, $grid) ;
      }
      else {
        $classes[] = runvsc_build_layout_classes($variables['layouts']) ;
  }
  $classes[] = $addclass ;
  $output = 'class="' . runvsc_implode_array($classes) . '"' ;
  return $output ;
}

function runvsc_build_footer_classes($layouts, $addclass = '') {
  $classes[] = runvsc_build_layout_classes($layouts) ;
  $classes[] = $addclass ;
  $output = 'class="' . runvsc_implode_array($classes) . '"' ;
  return $output ;
}

/**
* This is how the logo gets added
*/
function runvsc_build_logo() {
  $classes[] = runvsc_build_layout_classes($layouts) ;
  $title = variable_get('site_name', 'Drupal') ;
  $region_content = '<a href="' . base_path() . '"><img src="' . theme_get_setting('logo') . '" title="' . $title . '"/></a>' ;
  if ($region_content) {
    $output = runvsc_build_div_markup("logo-content", $classes, $region_content) ;
    return $output ;
  }
}

/**
* This is how the site name gets built
*/
function runvsc_build_site_name($layouts) {
  $classes[] = runvsc_build_layout_classes($layouts, "site_name") ;
  $title = theme_get_setting('toggle_name') ? filter_xss_admin(variable_get('site_name', 'Drupal')) : '' ;
  if ($title) {
    $region_content = '<a href="' . base_path() . '">' . $title . '</a>' ;
    $output = runvsc_build_div_markup("site-name", $classes, $region_content) ;
    return $output ;
  }
}

/**
* This is how the primary links gets added
*/
function runvsc_build_primary_links($layouts) {
// $output = theme_get_setting('toggle_primary_links') ? menu_primary_links() : array();
  $classes[] = runvsc_build_layout_classes($layouts, "primary") ;
  $region_content = theme('links', menu_primary_links(), array('class' => 'links', 'id' => 'navlist')) ;
  if ($region_content) {
    $output = runvsc_build_div_markup("primary-links", $classes, $region_content) ;
    return $output ;
  }
}

/**
* This is how the secondary gets added
*/
function runvsc_build_secondary_links($layouts) {
//$output = theme_get_setting('toggle_secondary_links') ? menu_secondary_links() : array();
  $classes[] = runvsc_build_layout_classes($layouts, "secondary") ;
  $region_content = theme('links', menu_secondary_links(), array('class' => 'links', 'id' => 'navlist')) ;
  if ($region_content) {
    $output = runvsc_build_div_markup("secondary-links", $classes, $region_content) ;
    return $output ;
  }
}

/**
* This is how the left region gets added
* remember that you can pass additional classes to the left or right
* from the subtheme_define_layouts function!
*/
function runvsc_build_left($region_content, $layouts) {
  $classes[] = runvsc_build_layout_classes($layouts, 'left', 'sidebar') ;
  $classes[] = 'sidebar' ;
  if ($region_content) {
    $output = runvsc_build_div_markup("left", $classes, $region_content, true) ;
    return $output ;
  }
}

/**
* This is how the right region gets added
*/
function runvsc_build_right($region_content, $layouts) {
  $classes[] = runvsc_build_layout_classes($layouts, 'right', 'sidebar') ;
  $classes[] = 'sidebar' ;
  if ($region_content) {
    $output = runvsc_build_div_markup("right", $classes, $region_content, true) ;
    return $output ;
  }
}

/**
* begin page.tpl functions <-- functions that generate markup displayed on the
* actual site pages. all the functions below are called from runvsc_preprocess_page
* and either set a value or return markup to be printed on the page.tpl
*/
function runvsc_build_head_title() {
// Construct page title
  if (drupal_get_title()) {
    $head_title = array(strip_tags(drupal_get_title()), variable_get('site_name', 'Drupal')) ;
  }
  else {
    $head_title = array(variable_get('site_name', 'Drupal')) ;
    if (variable_get('site_slogan', '')) {
      $head_title[] = variable_get('site_slogan', '') ;
    }
  }
  $output = implode(' | ', $head_title) ;
  return $output ;
}

function runvsc_build_site_slogan() {
  $output = theme_get_setting('toggle_slogan') ? filter_xss_admin(variable_get('site_slogan', '')) : '' ;
  return $output ;
}

function runvsc_build_search_box() {
  $output = theme_get_setting('toggle_search') ? runvsc_build_div_markup("search-box", array('true-block'), drupal_get_form('search_theme_form'), true) : '' ;
  return $output ;
}

function runvsc_build_mission() {
// Set mission when viewing the frontpage.
  if (drupal_is_front_page()) {
    $mission = filter_xss_admin(theme_get_setting('mission')) ;
  }
  $output = isset ($mission) ? $mission : '' ;
  return $output ;
}

function runvsc_build_top($region_content, $classes = null) {
  if ($region_content) {
    $output = runvsc_build_div_markup("top", $classes, $region_content, true) ;
    return $output ;
  }
}

function runvsc_build_header_top($region_content, $classes = null) {
  if ($region_content) {
    $output = runvsc_build_div_markup("header-top", $classes, $region_content, true) ;
    return $output ;
  }
}

function runvsc_build_header_content($region_content, $classes = null) {
  if ($region_content) {
    $output = runvsc_build_div_markup("header-content", $classes, $region_content, true) ;
    return $output ;
  }
}

function runvsc_build_page_title() {
  $classes = array("page-title", "title") ;
  $output = '<h1 class="' . runvsc_implode_array($classes) . '">' . drupal_get_title() . '</h1>' ;
  $output = runvsc_build_div_markup("page-title", $classes, $output) ;
  return $output ;
}

function runvsc_build_breadcrumb() {
// using the theme function @ http://api.drupal.org/api/function/theme/6
// using drupal_get_breadcrumb @ http://api.drupal.org/api/function/drupal_get_breadcrumb/6
  $output = theme('breadcrumb', drupal_get_breadcrumb()) ;
  return $output ;
}

function runvsc_build_preface_top($region_content) {
  $classes = array() ;
  if ($region_content) {
    $output = runvsc_build_div_markup("preface-top", $classes, $region_content, true) ;
    return $output ;
  }
}

function runvsc_build_preface_bottom($region_content) {
  $classes = array() ;
  if ($region_content) {
    $output = runvsc_build_div_markup("preface-bottom", $classes, $region_content, true) ;
    return $output ;
  }
}

/**
* Isn't it strange how the tabs on node / edit pages are called 'local tasks'? This makes sense to me now as those "tabs" actually define local tasks --- but man looking for it was really a pain.
*/
function runvsc_build_tabs() {
  $output = theme('menu_local_tasks') ;
  return $output ;
}

function runvsc_build_messages($show_messages = null) {
 if($show_messages == 1){  
  $classes[] = "true-block" ;
  $messages = runvsc_status_messages();
   if ($messages) {
    $output = runvsc_build_div_markup("messages", $classes, $messages, true) ;
    return $output ;
   }
  }
}

function runvsc_status_messages($display = null) {
  $classes[] = "true-block";
  foreach (drupal_get_messages($display) as $type => $messages) {
    if (count($messages) > 1) {
      $content = " <ul>\n" ;
      foreach ($messages as $message) {
        $content .= '  <li>' . $message . "</li>\n" ;
      }
      $content .= " </ul>\n" ;
    }
    else {
      $content .= $messages[0];
    }
  }
  $output = $content ;
  return $output ;
}

/**
* Some functions in this file are as simple as the one below. I put it in here so you don't have to look to hard to find it.
*/
function runvsc_build_help() {
  $output = theme('help') ;
  return $output ;
}

function runvsc_build_content_top($region_content, $classes = null) {
  if ($region_content) {
    $output = runvsc_build_div_markup("content-top", $classes, $region_content, true) ;
    return $output ;
  }
}

/**
* This is the function that pisses everyone off -> as you expect the $content variable to help you out. When you "dsm" it (which only works when you have the Devel modual enabled), you only see the html markup. Well, how do you alter that content? The answer is either use CCK modules or create a module with hook_nodeapi to alter that text.
*/
function runvsc_build_content($content, $classes = null) {
  if ($content) {
    $output = runvsc_build_div_markup("main-content", $classes, $content, true) ;
    return $output ;
  }
}

function runvsc_build_content_bottom($region_content, $classes = null) {
  if ($region_content) {
    $output = runvsc_build_div_markup("content-bottom", $classes, $region_content, true) ;
    return $output ;
  }
}

function runvsc_build_postscript_top($region_content, $classes = null) {
  if ($region_content) {
    $output = runvsc_build_div_markup("postscript-top", $classes, $region_content, true) ;
    return $output ;
  }
}

function runvsc_build_postscript_bottom($region_content, $classes = null) {
  if ($region_content) {
    $output = runvsc_build_div_markup("postscript-bottom", $classes, $region_content, true) ;
    return $output ;
  }
}

function runvsc_build_feed_icons($classes = null) {
  $content = drupal_get_feeds() ;
  if ($content) {
    $output = runvsc_build_div_markup("feed-icons", $classes, $content) ;
    return $output ;
  }
}

function runvsc_build_footer_message($classes = null) {
  $region_content = filter_xss_admin(variable_get('site_footer', false)) ;
  if ($region_content) {
    $output = runvsc_build_div_markup("footer-message", $classes, $region_content, true) ;
    return $output ;
  }
}

function runvsc_build_footer_content($region_content, $classes = null) {
  if ($region_content) {
    $output = runvsc_build_div_markup("footer-content", $classes, $region_content, true) ;
    return $output ;
  }
}

function runvsc_build_bottom($region_content, $classes = null) {
  if ($region_content) {
    $output = runvsc_build_div_markup("footer-content", $classes, $region_content, true) ;
    return $output ;
  }
}

/**
* end of page.tpl functions
*/
/**
* The huge function sets up the links for a site. You might not want to change this as it is pretty tight.
* Look at the other link functions below this!
*/
function runvsc_links($links, $attributes = array('class' => 'links')) {
  global $language ;
  $output = '' ;
  if (count($links) > 0) {
    $output = '<ul' . drupal_attributes($attributes) . '>' ;
    $num_links = count($links) ;
    $i = 1 ;
    foreach ($links as $key => $link) {
      $class = 'menu-item-' . runvsc_id_safe($link['title']) ;
      $class .= ' ' . $key ;
// Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first' ;
      }
      if ($i == $num_links) {
        $class .= ' last' ;
      }
      if (isset ($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page())) && (empty ($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active' ;
      }
      $output .= '<li' . drupal_attributes(array('class' => $class)) . '>' ;
      if (isset ($link['href'])) {
// Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link) ;
      }
      else
        if (!empty ($link['title'])) {
// Some links are actually not links, but we wrap these in <span> for adding title and class attributes
          if (empty ($link['html'])) {
            $link['title'] = check_plain($link['title']) ;
          }
          $span_attributes = '' ;
          if (isset ($link['attributes'])) {
            $span_attributes = drupal_attributes($link['attributes']) ;
          }
          $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>' ;
        }
        $i++;
      $output .= "</li>\n" ;
    }
    $output .= '</ul>' ;
  }
  return $output ;
}

/**
* These two menu function below just rock. The first adds classes to the li in a link set.  Remember that Drupal loves to use lists - and most menu items (created by the admin menu) exploit the use to lists:
*/
function runvsc_menu_item($link, $has_children, $menu = '', $in_active_trail = false, $extra_class = null) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf')) ;
  if (!empty ($extra_class)) {
    $class .= ' ' . $extra_class ;
  }
  if ($in_active_trail) {
    $class .= ' active-trail' ;
  }
  return '<li class="' . $class . '">' . $link . $menu . "</li>\n" ;
}

/**
* An addition to the function below is the "id safe" version of the page title to the class. This is useful to thmeing primary/secondary links.
*/
function runvsc_menu_item_link($link) {
  if (empty ($link['options'])) {
    $link['options'] = array() ;
  }
  $menu_class_title = 'menu-item-' . runvsc_id_safe($link['title']) ;
  $classes = array($menu_class_title) ;
// If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab">' . check_plain($link['title']) . '</span>' ;
    $link['options']['html'] = true ;
  }
  if (empty ($link['type'])) {
    $true = true ;
  }
  $link['options']['attributes'] = array('class' => runvsc_implode_array($classes, " ")) ;
  return l($link['title'], $link['href'], $link['options']) ;
}

/**
* begin node.tpl functions
*/
function runvsc_build_node_id($node) {
  $output = 'id="nid-' . $node->nid . '" ' ;
  return $output ;
}

function runvsc_build_node_classes($variables) {
  $classes = array() ;
  $classes[] = ($variables['sticky']) ? "sticky" : "" ;
  $classes[] = ($variables['status']) ? "" : "unpublished" ;
  $classes[] = ($variables['teaser']) ? "teaser" : "" ;
  $classes[] = "true-block" ;
  $output = 'class="' . runvsc_implode_array($classes) . '" ' ;
  return $output ;
}

/**
* The "picture" in a node isn't any old picture, it is the users picture. strange, i know. This was been corrected in 7
*/
function runvsc_build_node_picture($node) {
  if (theme_get_setting('toggle_node_user_picture' . $node->type)) {
    $classes = array("image") ;
    $content = theme('user_picture', $node) ;
    $output = runvsc_build_div_markup("user-picture", $classes, $content, true) ;
  }
  else {
    $output = "" ;
  }
  return $output ;
}

function runvsc_build_node_title_for_teaser($node) {
  $title = check_plain($node->title) ;
}

function runvsc_build_node_title($variables) {
  if ($variables['teaser'] && $variables['node']->teaser) {
    $classes = array("teaser-title", "title") ;
    $title = check_plain($variables['node']->title) ;
    $output = '<h2 class="' . runvsc_implode_array($classes) . '"><a href="' . $variables['node_url'] . '" title="' . $title . '">' . $title . '</a></h2>' ;
  }
  else {
// remember if this isn't a teaser, the title is handled by the page.tpl!
    $output = "" ;
  }
  return $output ;
}

/**
* Ok, this function below determines whether we should show "teaser" content or the full node. The best way to leverage this is by creating a preprocessing function in the sub-theme tpl that generates the teaser content or the  body content; then add it to the node as indicated by the "$variables['node']->teaser". notice how i set it through a runvsc_build_div_markup so that you don't have to ?!
*/
function runvsc_build_node_content($variables) {
  $classes = array('clear-block') ;
  if ($variables['teaser'] && $variables['node']->teaser) {
    $output = runvsc_build_div_markup("content", $classes, $variables['node']->teaser) ;
  }
  elseif (isset ($variables['node']->body)) {
    $output = $output = runvsc_build_div_markup("content", $classes, $variables['node']->body) ;
  }
  else {
    $output = '' ;
  }
  return $output ;
}

/**
* These are actually the "$terms" that get printed out. Methinks that they are passed as an array.
*/
function runvsc_build_node_taxonomy($node) {
  if (module_exists('taxonomy')) {
    $output = taxonomy_link('taxonomy terms', $node) ;
  }
  else {
    $output = array() ;
  }
  return $output ;
}

/**
* These are the terms generated as links.
*/
function runvsc_build_node_terms($variables) {
  $output = theme('links', $variables['taxonomy'], array('class' => 'terms links inline')) ;
  return $output ;
}

function runvsc_build_node_submitted($node) {
  $content = runvsc_node_submitted($node) ;
  $classes = array('submitted') ;
  if (theme_get_setting('toggle_node_info_' . $node->type)) {
    $output = runvsc_build_div_markup("node-submitted", $classes, $content) ;
    return $output ;
  }
}

function runvsc_build_node_links($node) {
  if (!empty ($node->links)) {
    $output = theme('links', $node->links, array('class' => 'terms links inline')) ;
  }
  else {
    $output = "" ;
  }
  return $output ;
}

/**
* end node.tpl functions
*/
/**
*  begin block.tpl functions
*/
function runvsc_build_block_id($variables) {
  $bid = array('block-' . $variables['block']->module . '-' . $variables['block_id']) ;
  $output = 'id="' . runvsc_implode_array($bid) . '"' ;
  return $output ;
}

function runvsc_build_block_classes($variables) {
    /* to do : remember to add skinr */
  $classes = array("block", "block-" . $variables['block']->module) ;
  // adding snippet for  http://drupal.org/project/block_class
  $block_class_variables = (function_exists('block_class')) ? ' '.block_class($variables['block']) : '';
  $output = 'class="' . runvsc_implode_array($classes).$block_class_variables. '"' ;
  return $output ;
}

function runvsc_build_block_title($title) {
  $classes = array("title") ;
  if ($title) {
    $title = '<h2>' . $title . '</h2>' ;
    $output = runvsc_build_div_markup("block-title", $classes, $title) ;
    return $output ;
  }
}

function runvsc_build_block_content($content) {
  $classes = array() ;
  if ($content) {
    $output = runvsc_build_div_markup("block-content", $classes, $content, true) ;
    return $output ;
  }
}

/**
* end block.tpl functions
*/
/**
*  begin comment.tpl functions
*/
function runvsc_build_comment_new($content) {
  $classes = array('new') ;
  if ($content) {
    $output = runvsc_build_div_markup("comment-new", $classes, "") ;
    return $output ;
  }
}

function runvsc_build_comment_title($title) {
  $classes = array("title") ;
  if ($title) {
    $title = '<h3>' . $title . '</h3>' ;
    $output = runvsc_build_div_markup("comment-title", $classes, $title) ;
    return $output ;
  }
}

function runvsc_build_comment_content($content) {
  $classes = array('content', 'clear-block') ;
  if ($content) {
    $output = runvsc_build_div_markup("comment-content", $classes, t($content)) ;
    return $output ;
  }
}

function runvsc_build_comment_submitted($content) {
  $content = runvsc_comment_submitted($content) ;
  $classes = array('submitted') ;
  if ($content) {
    $output = runvsc_build_div_markup("comment-submitted", $classes, $content) ;
    return $output ;
  }
}

function runvsc_build_comment_signature($content) {
  $classes = array('user-signature', 'clear-block') ;
  if ($content) {
    $output = runvsc_build_div_markup("comment-signature", $classes, $content) ;
    return $output ;
  }
}

function runvsc_build_comment_links($content) {
  $classes = array('links') ;
  if (isset ($content)) {
    $content = theme('links', $content) ;
    $output = runvsc_build_div_markup("comment-links", $classes, $content) ;
    return $output ;
  }
}

/**
* Theme a "Submitted by ..." notice.
*
* @param $comment
*   The comment.
* @ingroup themeable
*/
function runvsc_comment_submitted($comment) {
  if ($comment) {
    $username = theme('username', $comment) ;
    $timestamp = format_date($comment->timestamp) ;
    $text = t('Submitted by ' . $username . ' on') ;
    $output = runvsc_build_tag_markup("span", null, "username", $text) . '&nbsp;' . runvsc_build_tag_markup("span", null, "timestamp", $timestamp) ;
    return $output ;
  }
}

/**
* Format the "Submitted by username on date/time" for each node
*
* @ingroup themeable
*/
function runvsc_node_submitted($node) {
  $username = theme('username', $node) ;
  $timestamp = format_date($node->created) ;
  $text = t('Submitted by ' . $username . ' on') ;
  $output = runvsc_build_tag_markup("span", null, "username", $text) . '&nbsp;' . runvsc_build_tag_markup("span", null, "timestamp", $timestamp) ;
  return $output ;
}

function runvsc_image_node_background($node) {
// get path of the image
  $image_path = base_path() . $node['images']['front page slide show'];
// create wrapper and then build the content inside of it!
  $new_content = $node['field_image_body'][0]['safe'];
  $wrapper = '<div id="image-wrapper" style="background-image:url(' . $image_path . ')">' . $new_content . '</div>' ;
  return $wrapper ;
}

/**
* This function return a date in the form of those cool
* mon calendars! Just pass the date to it....
* and add a "date block" to the beginning of the content.
*  To be executed from  preprocess_node.
*
* @param $vars
*/
function runvsc_return_date_block($date) {
//convert string to a UNIX timestamp!
  $date = strtotime($date) ;
  $day = date('d', $date) ;
  $month = date('F', $date) ;
  $year = date('Y', $date) ;
  $date_block = '<div class="date-block">' ;
  $date_block .= '<span class="month">' . $month . '</span>' ;
  $date_block .= '<span class="day">' . $day . '</span>' ;
  $date_block .= '<span class="year">' . $year . '</span>' ;
  $date_block .= '</div>' ;
  $output = $date_block . $vars['content'];
  return $output ;
}



/**
* Implementation of Ninesplice
* parameters:
*   class : add a class to target with the ninesplice css
*   title : by passing a title, it will be shown in the top row, center column
*   body : this is the content that you want to display ... ok to show title here, too
*   css : if you don't register the ninesplice.css in the .info file because you are only using this block on a page or two, set this value to TRUE when you call it and it will load the ninesplice.css only for that page :)
*/
function runvsc_ninesplice($class = 'default', $title = '', $body, $css = null) {
  $class = (!$class) ? 'default' : $class ;
  if ($css) {
    $pathtotheme = path_to_theme() ;
    drupal_add_css($pathtotheme . '/ninesplice/ninesplice.css') ;
  }
  //dsm('zen_more_ninesplice = '.$class);
  if($class != 'skip'){
    $title_print = ($title) ? $title : '' ;
    $output = '<table id="ninesplice" class="' . $class . '">' ;
    $output .= '  <tr>' ;
    $output .= '    <td class="ninesplice-1"> </td>' ;
    $output .= '    <td class="ninesplice-2">' . $title_print . '</td>' ;
    $output .= '    <td class="ninesplice-3"></td>' ;
    $output .= '  </tr>' ;
    $output .= '  <tr>' ;
    $output .= '    <td class="ninesplice-4"></td>' ;
    $output .= '    <td class="ninesplice-5">' . $body . '</td>' ;
    $output .= '    <td class="ninesplice-6"></td>' ;
    $output .= '  </tr>' ;
    $output .= '  <tr>' ;
    $output .= '    <td class="ninesplice-7"></td>' ;
    $output .= '    <td class="ninesplice-8"></td>' ;
    $output .= '    <td class="ninesplice-9"></td>' ;
    $output .= '  </tr>' ;
    $output .= '</table>' ;
   return $output ;
  }else{
    return $body;
  }
}

/**
 * This is pinched (almost) verbatium from Zen. Thank you Zen maintainers esp jjeff and JohnAlbin - 
 * I really could not have written it better.
 */
function runvsc_build_admin_block_links(&$variables){
  $block = $variables['block'];
   // Display 'edit block' for custom blocks.
  if ($block->module == 'block') {
    $edit_links[] = l('<span>' . t('edit block') . '</span>', 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
      array(
        'attributes' => array(
          'title' => t('edit the content of this block'),
          'class' => 'block-edit',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }
  // Display 'configure' for other blocks.
  else {
    $edit_links[] = l('<span>' . t('configure') . '</span>', 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
      array(
        'attributes' => array(
          'title' => t('configure this block'),
          'class' => 'block-config',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }

  // Display 'edit view' for Views blocks.
  if ($block->module == 'views' && user_access('administer views')) {
    $edit_links[] = l('<span>' . t('edit view') . '</span>', 'admin/build/views/' . $block->delta . '/edit',
      array(
        'attributes' => array(
          'title' => t('edit the view that defines this block'),
          'class' => 'block-edit-view',
        ),
        'query' => drupal_get_destination(),
        'fragment' => 'edit-block',
        'html' => TRUE,
      )
    );
  }
  // Display 'edit menu' for Menu blocks.
  elseif (($block->module == 'menu' || ($block->module == 'user' && $block->delta == 1)) && user_access('administer menu')) {
    $menu_name = ($block->module == 'user') ? 'navigation' : $block->delta;
    $edit_links[] = l('<span>' . t('edit menu') . '</span>', 'admin/build/menu-customize/' . $menu_name,
      array(
        'attributes' => array(
          'title' => t('edit the menu that defines this block'),
          'class' => 'block-edit-menu',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }
  // Display 'edit menu' for Menu block blocks.
  elseif ($block->module == 'menu_block' && user_access('administer menu')) {
    $menu_name = variable_get('menu_block_' . $block->delta . '_menu_name', 'navigation');
    $edit_links[] = l('<span>' . t('edit menu') . '</span>', 'admin/build/menu-customize/' . $menu_name,
      array(
        'attributes' => array(
          'title' => t('edit the menu that defines this block'),
          'class' => 'block-edit-menu',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }

  $variables['edit_links_array'] = $edit_links;  
  $output = implode(' | ', $edit_links);
  $variables['admin_links'] = runvsc_build_div_markup("block-admin-links", null, $output, TRUE);  
}